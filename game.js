(function(NS) {
  var World = NS.World;

  var Game = function() {
    this.levelList = [
      { name: 'level0' },
      { name: 'level1' },
      { name: 'level2' },
      { name: 'level5' },
      { name: 'level4' },
      { name: 'level3' },
      { name: 'sokoban' },
      { name: 'level6' },
      { name: 'beta-intro' },
      { name: 'level7' },
      { name: 'sensor-maze' },
      { name: 'bit-random' },
      { name: 'radar-maze' },
      { name: 'temple' },
      { name: 'robot-duel' }
    ];
    this.currentLevel = undefined;
    this.editor = undefined;
    this.darkness = 0;
    this.levels = [];
    this.levelNumber = undefined;
    this.page = 'index.html';
  }

  Console = function(element) {
    this.element = element;
  }

  Console.prototype = {
    time: 0,
    formatError: _.template('<div><div><%= time %></div><div class="error"><%=name%>: <%= message %></div></div>'),
    format: _.template('<div><div><%= time %></div><div><%= message %></div></div>'),
    console: window.console,
    entries: [],
    dirty: false,
    clear: function() {
      this.entries = [];
      this.display();
    },
    log: function(what) {
      var entries = this.entries;
      while (entries.length > 100) {
        entries.shift();
      }
      if (what instanceof Error) {
        entries.push(this.formatError({ time: this.time, name: what.name, message: what.message }));
      } else {
        entries.push(this.format({ time: this.time, message: what }));
      }

      this.dirty = true;
    },
    setTime: function(time) { 
      this.time = time;
    },
    display: function() {
      if (!this.dirty) {
        return;
      }

      var entries = this.entries;
      var html = [];
      for (var i = 0; i < entries.length; i++) {
        html.push(entries[i]);
      }
      this.element.innerHTML = html.join('');
      this.element.scrollTop = this.element.scrollHeight;
      this.dirty = false;
    }
  };

  Game.prototype = {
    _createCanvas: function(parentElement, width, height, className) {
      var canvas = document.createElement('canvas');
      canvas.className = className;
      canvas.width=width;
      canvas.height=height;

      parentElement.appendChild(canvas);

      return canvas;
    },


    setup: function() {
      $.ajaxSetup({
        cache: false
      });


      var container = document.getElementById('canvas_container');
      var contexts = {
        background: this._createCanvas(container, 600, 600, 'game_canvas').getContext('2d'),
        world: this._createCanvas(container, 600, 600, 'game_canvas').getContext('2d'),
        overlay: this._createCanvas(container, 600, 600, 'game_canvas').getContext('2d'),
        shadow: this._createCanvas(container, 600, 600, 'shadow_canvas hidden').getContext('2d')
      };
      this.contexts = contexts;


      var overlay = '<div class="content"></div><div class="opacity_mask"></div>';
      this.$overlay = $(overlay).appendTo($('#overlay_div'));

      $(window).on('hashchange', this.loadLevel.bind(this));


      this.$overlay.delegate('.next', 'click', this.nextLevel.bind(this));
      this.$overlay.delegate('.replay', 'click', this.loadLevel.bind(this));


      this.editor = CodeMirror(document.getElementById('editarea'), {
        value: '',
        mode:  "javascript",
        matchBrackets: true,
        lineNumbers: true
      });


      $('#run_robots_run').on('click', this.onRun.bind(this));
      $('#stop_robots_stop').on('click', this.onStop.bind(this));
      $('#edit_robots_edit').on('click', this.onEdit.bind(this));
      $('#view_robots_view').on('click', this.onView.bind(this));

      this.loadLevel();

      this.console = new Console(document.getElementById('logs'));
      this.view();
    },

    nextLevel: function() {
      this.selectLevel(this.levelNumber + 1);
    },


    selectLevel: function(number) {
      window.location = this.page + '#' + Math.min(number, this.levelList.length - 1);
    },

    getLevelNumber: function() {
      var hash = window.location.hash;
      if (hash[0] == '#') {
        return Math.min(hash.substring(1)|0, this.levelList.length - 1);
      } else {
        return -1;
      }
    },


    resetDefault: function() {
      var editor = this.editor;
      $.ajax({
        url: "levels/" + this.level.name + ".txt",
        success: function( data ) {
          editor.getDoc().setValue(data);
        }
      });
    },

    setDarkness: function(value) {
      this.darkness = value;
      if (this.world.setDarkness) {
        this.world.setDarkness(value);
        if (this.world.stopped) { 
          this.world.render();
        }
      }
    },


    onRun: function() {
      this.run();
    },

    onView: function() {
      this.view();
    },

    onEdit: function() {
      this.edit();
    },

    onStop: function() {
      this.stop();
    },

    view: function() {
    },

    edit: function() {
    },

    run: function() {
      window.console = this.console;
      this.view();
      // Stop anything going on in the old world
      this.stop();

      var code = this.editor.getDoc().getValue();

      localStorage.setItem('robot-code-' + this.level.name, code); 

      // reinit level
      var world = new World(this.contexts, this.level);
      world.findObject('player').setCode(code);
      if (world.setDarkness) {
        world.setDarkness(this.darkness);
      }
      world.win = this.win.bind(this);
      world.lose = this.lose.bind(this);

      this.world = world;
      this.world.start();

    },

    stop: function() {
      if (this.world !== undefined) {
        this.world.stopped = true;
      }
    },

    setEditorContent: function() {
      var code = localStorage.getItem('robot-code-' + this.level.name);
      if (code == undefined) {
        this.resetDefault();
      } else {
        this.editor.getDoc().setValue(code);
      }
    },

    addLevel: function(level) {
      this.levels[level.name] = level;

      // After load?
      if (this.levelNumber !== undefined && level.name === this.levelList[this.levelNumber].name) {
        this.showLevel();
      }
    },

    loadLevel: function() {
      this.stop();
      var number = this.getLevelNumber();

      if (this.levelNumber === undefined) {
        this.levelNumber = 0;
      }

      if (number == -1) {
        this.selectLevel(this.levelNumber);
        return;
      }

      this.levelNumber = number;
      var name = this.levelList[number].name;

      if (this.levels[name] == undefined) {
        $.getScript("levels/" + name + '.js').fail(function(jqxhr, settings, exception) {
          console.log(exception);
        });
      } else {
        setTimeout(this.showLevel.bind(this), 0);
      }
    }, 

    showLevel: function() {
      this.contexts.world.setTransform(1, 0, 0, 1, 0.5, 0.5);
      this.contexts.shadow.setTransform(1, 0, 0, 1, 0.5, 0.5);
      this.contexts.overlay.setTransform(1, 0, 0, 1, 0.5, 0.5);

      var levelName = this.levelList[this.levelNumber].name;
      this.level = this.levels[levelName];


      var title = '#' + this.levelNumber + ' - ' + this.level.title || levelName;
      document.getElementById('levelTitle').innerHTML = title;

      this.setEditorContent();

      window.console = this.console;

      var world = new World(this.contexts, this.level, '');
      if (world.setDarkness) {
        world.setDarkness(this.darkness);
      }
      this.world = world;

      try {
        world.render();
      } catch (e) {
        console.log(e);
        return;
      }
      this.hideOverlay();

      this.view();
    },

    showOverlay: function(html) {
      $('#overlay_div').children('.content').html(html);
      $('#overlay_div').removeClass('hidden');
    },

    hideOverlay: function() {
      $('#overlay_div').addClass('hidden');
    },

    lose: function() {
      this.stop();
      this.showOverlay('<h2>Oh, man. You suck.</h2><a class="aui-button aui-button-primary aui-style replay">Retry</a>');
    },

    win: function() {
      this.stop();
      this.showOverlay('<h2>Level complete</h2><a class="aui-button aui-style replay">Replay</a> <a class="aui-button aui-button-primary aui-style next">Next level</a>');
    }
  }

  var game = new Game();
  $(document).ready(function(){ 
    game.setup();
  });
  NS.game = game;
}(window));
